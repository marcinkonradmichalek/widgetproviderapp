package pl.michalek.marcin.widgetproviderapp;

import android.app.Application;
import com.orhanobut.hawk.Hawk;

/**
 * Created by marcinm on 1/6/18.
 */

public class App extends Application {
  @Override
  public void onCreate() {
    super.onCreate();
    Hawk.init(this).build();
  }
}
