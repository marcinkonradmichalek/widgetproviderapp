package pl.michalek.marcin.widgetproviderapp.activity

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v7.app.AppCompatActivity
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_widget_configuration.*
import pl.michalek.marcin.widgetproviderapp.R
import pl.michalek.marcin.widgetproviderapp.widget.DemoAppWidgetProvider

/**
 * Created by marcinm on 1/6/18.
 */
class WidgetConfigurationActivity : AppCompatActivity() {

    companion object {
        const val KEY_SELECTED_IMAGE_RES_ID = "KEY_SELECTED_IMAGE_RES_ID"
    }

    private var appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_widget_configuration)
        if (intent.extras != null) {
            appWidgetId = intent.extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
        }
        setInitialCancelledResult()
        imageOne.setOnClickListener { storeSelectedImageResIdAndFinish(R.drawable.palm_trees_one) }
        imageTwo.setOnClickListener { storeSelectedImageResIdAndFinish(R.drawable.palm_trees_two) }
    }

    private fun setInitialCancelledResult() {
        val resultValue = Intent()
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        setResult(Activity.RESULT_CANCELED, resultValue)
    }

    private fun storeSelectedImageResIdAndFinish(@DrawableRes imageResId: Int) {
        Hawk.put(KEY_SELECTED_IMAGE_RES_ID, imageResId)
        updateWidgetUi()
        setSuccessViewResult()
        finish()
    }

    private fun updateWidgetUi() {
        val ids = AppWidgetManager.getInstance(this).getAppWidgetIds(ComponentName(this, DemoAppWidgetProvider::class.java))
        val myWidget = DemoAppWidgetProvider()
        myWidget.onUpdate(this, AppWidgetManager.getInstance(this), ids)
    }

    private fun setSuccessViewResult() {
        val resultValue = Intent()
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        setResult(Activity.RESULT_OK, resultValue)
        finish()
    }
}