package pl.michalek.marcin.widgetproviderapp.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.Toast
import pl.michalek.marcin.widgetproviderapp.R


/**
 * Created by marcinm on 1/6/18.
 */
class DemoAppWidgetProvider : AppWidgetProvider() {
    companion object {
        const val TOAST_ACTION = "ACTION_TOAST"
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == TOAST_ACTION) {
            val viewPosition = intent.getIntExtra(DemoWidgetListProvider.KEY_POSITION, 0)
            Toast.makeText(context, context.getString(R.string.touched_format, viewPosition), Toast.LENGTH_SHORT).show()
        } else {
            super.onReceive(context, intent)
        }
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        appWidgetIds.forEach { id ->
            val views = RemoteViews(context.packageName, R.layout.demo_appwidget)
            val listViewRemoteViewService = Intent(context, DemoWidgetService::class.java)
            views.setRemoteAdapter(R.id.imagesList, listViewRemoteViewService)

            val toastIntent = Intent(context, DemoAppWidgetProvider::class.java)
            toastIntent.action = TOAST_ACTION
            toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id)
            val toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT)
            views.setPendingIntentTemplate(R.id.imagesList, toastPendingIntent)

            appWidgetManager.notifyAppWidgetViewDataChanged(id, R.id.imagesList)
            appWidgetManager.updateAppWidget(id, views)
        }
    }
}