package pl.michalek.marcin.widgetproviderapp.widget

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import pl.michalek.marcin.widgetproviderapp.R


/**
 * Created by marcinm on 1/6/18.
 */

class DemoWidgetListProvider(private val applicationContext: Context) : RemoteViewsService.RemoteViewsFactory {

    companion object {
        const val ITEM_VIEW_TYPE_COUNT = 1
        const val KEY_POSITION = "KEY_POSITION"
    }

    private var data = listOf(R.drawable.palm_trees_one, R.drawable.palm_trees_two)

    override fun onCreate() {
        //no op
        //factory created
    }

    override fun onDataSetChanged() {
        //no op
        //called when notifyDataSetChanged() called
    }

    override fun onDestroy() {
        //no op
        //factory unbound
    }

    override fun getCount() = data.size


    override fun getViewAt(position: Int): RemoteViews {
        val remoteView = RemoteViews(applicationContext.packageName, R.layout.item_demo_widget)
        remoteView.setImageViewResource(R.id.image, data[position])
        val extras = Bundle()
        extras.putInt(KEY_POSITION, position)
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)
        remoteView.setOnClickFillInIntent(R.id.image, fillInIntent)
        return remoteView
    }

    override fun getLoadingView() = RemoteViews(applicationContext.packageName, R.layout.item_widget_loading)

    override fun getViewTypeCount() = ITEM_VIEW_TYPE_COUNT

    override
    fun getItemId(position: Int) = position.toLong()

    override fun hasStableIds() = true
}
