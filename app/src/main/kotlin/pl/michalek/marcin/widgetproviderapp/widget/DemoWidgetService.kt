package pl.michalek.marcin.widgetproviderapp.widget

import android.content.Intent
import android.widget.RemoteViewsService

/**
 * Created by marcinm on 11/13/17.
 */

class DemoWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent) = DemoWidgetListProvider(applicationContext)
}
